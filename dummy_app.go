package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
)

const (
	passedTest = iota
	failedTest
	passedNoOutFilesTest
	failedNoOutFilesTest
)

const (
	ErrSetNotFound = iota
	ErrTestNotFound
	ErrUnexpected
)

const (
	DefaultQueue = "default"
)

//cmd flags

type Params struct {
	TestId   int
	SplitSet int
}

type Test struct {
	TestId int    `json:"test_id"`
	Queue  string `json:"queue"`
}

func (self *Test) ToSting() (string, error) {
	data, err := json.Marshal(self)
	if err != nil {
		log.Fatal("Cannot marshal the test into JSON")
		return "", err
	}
	return string(data), nil
}

// handler type

type Handler func(int) error

// handlers

func HandlePassedTest(id int) error {
	messages := make([]string, 0, 20)
	filename := fmt.Sprintf("test-%d.log", id)
	log.Printf(fmt.Sprintf("File name is: %s", filename))
	if _, err := os.Stat(filename); !os.IsNotExist(err) {
		log.Printf(fmt.Sprintf("File \"%s\" exists, removing...", filename))
		err := os.Remove(filename)
		if err != nil {
			return err
		}
		log.Printf(fmt.Sprintf("The OLD file \"%s\" has been removed", filename))
	}
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()
	logger := log.New(file, "", log.Ldate|log.Ltime|log.Lshortfile)
	for _, msg := range messages {
		file.WriteString(msg)
	}

	logger.Printf(fmt.Sprintf("Completed test %d", id))
	return nil
}

var tests map[int]map[int]Handler = map[int]map[int]Handler{
	1: {
		1: HandlePassedTest,
	},
}
var params Params

func init() {
	// tests = make(map[int]Hander)
	flag.IntVar(&params.TestId, "test-id", 0, "Test ID to be run")
	flag.IntVar(&params.SplitSet, "split-set", 0, "Split the given test set into the individual tests")
	flag.Parse()
}

func DoSplit() {
	set, ok := tests[params.SplitSet]
	if ok {
		for key := range set {
			test := new(Test)
			test.Queue = DefaultQueue
			test.TestId = key
			stringRes, err := test.ToSting()
			if err != nil {
				panic(err)
			}
			fmt.Println(stringRes)
		}
	} else {
		os.Exit(ErrSetNotFound)
	}
}

func RunTest(testID int) {
	for _, value := range tests {
		val, ok := value[params.TestId]
		if ok {
			fmt.Printf("..........\nExecuting test %d...\n", testID)
			err := val(testID)
			if err != nil {
				panic(err)
			}
			os.Exit(0)
		}
	}
}

func main() {
	defer func() {
		if err := recover(); err != nil {
			log.Printf("An unexpeced error has occurred: %s", err)
			os.Exit(ErrUnexpected)
		}
	}()
	switch {
	case params.SplitSet > 0:
		DoSplit()
		os.Exit(0)
	case params.TestId > 0:
		RunTest(params.TestId)
		os.Exit(0)
	default:
		os.Exit(ErrTestNotFound)
	}
}
