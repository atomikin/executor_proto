package git_manager

import (
	"errors"
	"github.com/atomikin/executor-commons"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"strings"
)

const (
	inProgress = iota
	ready
)

// var params *executor_commons.Params

// func init() {
// 	params = executor_commons.LoadParams()
// }

// repository absraction

/*
	repo - repository`s URI
	revision - hash tag, or some other ID
	path - the place where repo should be. May not exist
*/
type Repo struct {
	// WriteSem   chan bool
	// ReadSem    chan bool
	// Observers chan bool
	URI      string
	Revision string
	Branch   string
	Path     string
}

// func (self *Repo) setUp() {
// 	self.WriteSem = make(chan bool)
// 	self.ReadSem = make(chan bool, params.ExecutorsNumber - 1)
// }

// func (self *Repo) ReleaseRead(name string) {
// 	if len(self.ReadSem) > 0 {
// 		<- self.ReadSem
// 		if len(self.ReadSem) == 0 {
// 		}
// 	}
// }

type Interface interface {
	New() Interface

	Prepare(repo Repo) error
}

type Key struct {
	Executor int
	URL      string
}

type Manager struct {
	Repositories map[Key]*Repo
}

func (repo *Repo) Clone() error {
	cmd := exec.Command(
		"git", "clone",
		repo.URI,
		repo.Path,
	)
	// stderr, err := cmd.StderrPipe()
	err := cmd.Run()
	if err != nil {
		executor_commons.HandleError(err)
		return err
	}
	return nil
}

func (self *Repo) Checkout() error {
	checkout := exec.Command(
		"git", "checkout", self.Revision,
	)
	stderr, err := checkout.StderrPipe()
	err = checkout.Start()
	data, _ := ioutil.ReadAll(stderr)
	err = checkout.Wait()
	if err != nil {
		log.Printf("[Error] !%s:\n%s\n", err, data)
	}
	// LogIfError(err, &stderr)
	return err

}

func (self *Repo) Fetch() error {
	fetch := exec.Command(
		"git", "fetch", "origin", self.Branch,
	)
	stderr, err := fetch.StderrPipe()
	err = fetch.Start()
	if err != nil {
		data, _ := ioutil.ReadAll(stderr)
		err = fetch.Wait()
		executor_commons.FailOnError(err, "cannot fetch repo")
		log.Printf("[Error] %s:\n%s\n", err, data)
	}
	return err
}

func (self *Repo) IsUpToDate() bool {
	cmd := exec.Command(
		"git", "log", "-1",
	)
	stdout, err := cmd.StdoutPipe()
	executor_commons.FailOnError(err, "Failed to get Pipe")
	err = cmd.Start()
	executor_commons.FailOnError(err, "Unexpected err")
	out, err := ioutil.ReadAll(stdout)
	err = cmd.Wait()
	executor_commons.FailOnError(err, "Error reading the STDIN")
	return strings.Contains(string(out), self.Revision)
}

func (self *Repo) BranchIsOk() bool {
	cmd := exec.Command(
		"git", "rev-parse", "--abbrev-ref", "HEAD",
	)
	stdout, _ := cmd.StdoutPipe()
	err := cmd.Start()
	if err != nil {
		return false
	}
	out, _ := ioutil.ReadAll(stdout)
	err = cmd.Wait()
	// the error to be propagated
	executor_commons.FailOnError(err, "Cannot read stdout")
	return strings.Contains(string(out), self.Branch)
}

func (self *Manager) Prepare(repo *Repo) (bool, error) {
	// handle any errors...
	defer func() {
		if err := recover(); err != nil {
			executor_commons.HandleError(err.(error))
		}
	}()
	if _, err := os.Stat(repo.Path); os.IsNotExist(err) {
		err = os.MkdirAll(repo.Path, os.ModePerm)
		if err != nil {
			return false, err
		}
		err := os.Chdir(repo.Path)
		executor_commons.FailOnError(err, "Cannot switch dir")
		err = repo.Clone()
		err = repo.Fetch()
		err = repo.Checkout()
		if err != nil {
			return false, err
		} else {
			return true, nil
		}
	}
	return false, errors.New("Dir exists")

}
