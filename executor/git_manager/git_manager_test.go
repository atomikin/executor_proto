package git_manager

import (
	"github.com/atomikin/executor-commons"
	"log"
	"os"
	"testing"
)

func deleteIfExist(path string) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		log.Printf("Could not find dir: %s", path)
	} else {
		log.Printf("Deleting the target dir: %s", path)
		err = os.RemoveAll(path)
		executor_commons.FailOnError(err, "could not remove")
	}

}

func TestPrepareWhenNoRepoExist(t *testing.T) {
	repo := Repo{
		URI:      "https://atomikin@bitbucket.org/atomikin/executor_proto.git",
		Branch:   "master",
		Revision: "835032e23b3b029dd0e5065af6feebb9b925bc64",
		Path:     "/tmp/test",
	}
	defer deleteIfExist(repo.Path)
	mgr := new(Manager)
	done, err := mgr.Prepare(&repo)
	log.Printf("Created dir: %s", repo.Path)
	if done != true {
		t.Errorf("Faced error: %s", err)
	}
}
