package main

import (
	"encoding/json"
	"fmt"
	"github.com/atomikin/executor-commons"
	"github.com/streadway/amqp"
	"log"
	"time"
)

type Out struct {
	Feedback chan bool
	Job      *executor_commons.Job
}

func (self *Out) Complete() {
	<-self.Feedback
}

func main() {
	// params := executor_commons.LoadParams()

	stop := make(chan bool)
	out := make(chan *Out)
	go Receive(out, stop)
	// parallel := make(chan *Out, 2)
	for i := 0; i < 10; i++ {
		go func(i int) {
			for {
				res := <-out

				fmt.Printf(
					"[worker-%d] %d: repo - %s, revision: %s\n",
					i, res.Job.Id, res.Job.Tsk.Repository,
					res.Job.Tsk.Commmit,
				)
				time.Sleep(20 * time.Second)
				res.Complete()
			}
		}(i)
	}
	<-stop
}

func Receive(out chan *Out, stop chan bool) {
	params := executor_commons.LoadParams()
	conn, err := amqp.Dial(params.Rabbit())
	executor_commons.FailOnError(err, "[ERROR]: Cannot connect to server")
	defer conn.Close()
	ch, err := conn.Channel()
	executor_commons.FailOnError(err, "[ERROR]: Chan creation failed")
	defer func() {
		if err := recover(); err != nil {
			executor_commons.FailOnError(err.(error), "Unexpected Error")
		}
		ch.Close()
		log.Println("Channel closed")
	}()
	for _, i := range params.Queues {
		// fmt.Println((&i).ToString())
		q, err := ch.QueueDeclare(
			i.Name,
			false,
			false,
			true,
			false,
			nil,
		)
		executor_commons.FailOnError(err, "Failed to declare queue")
		err = ch.QueueBind(
			q.Name,  // queue name
			"",      // routing key
			"tasks", // exchange
			false,
			nil)
		executor_commons.FailOnError(err, fmt.Sprintf("Error binding queue", i.Name))
		go func(q amqp.Queue, qConf executor_commons.Queue) {
			buffer := make(chan bool, qConf.Limit)
			for {
				msgs, err := ch.Consume(
					q.Name, // queue
					"",     // consumer
					true,   // auto-ack
					false,  // exclusive
					false,  // no-local
					false,  // no-wait
					nil,    // args
				)
				executor_commons.HandleError(err)
				if err == nil {
					for m := range msgs {
						var msg = new(executor_commons.Job)
						err = json.Unmarshal(m.Body, msg)
						executor_commons.HandleError(err)
						if err == nil {
							outMsg := Out{Feedback: buffer, Job: msg}
							outMsg.Feedback <- true
							out <- &outMsg
						}
					}
				}
			}
		}(q, i)
	}
	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-stop
}
